package ua.danit.dao.jdbc;

import ua.danit.mappers.Mapper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DaoUtils {

    public final static DaoUtils DAO_UTILS = new DaoUtils();
    private DataSource dataSource;

    private DaoUtils() {
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public <T> T getItemFromQuery(String query, Mapper<T> mapper) {
        T item = null;

        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query)) {

            item = getItem(resultSet, mapper);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return item;
    }

    private <T> T getItem(ResultSet resultSet, Mapper<T> mapper) {
        T item = null;

        try (ResultSet rs = resultSet) {
            if (rs.next()) {
                item = mapper.map(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return item;
    }

    public <T> List<T> getItemListFromQuery(String query, Mapper<T> mapper) {
        List<T> item = null;

        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query)) {

            item = getItemList(resultSet, mapper);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return item;
    }

    private <T> List<T> getItemList(ResultSet resultSet, Mapper<T> mapper) {
        List<T> item = new ArrayList<>();

        try (ResultSet rs = resultSet) {
            while (rs.next()) {
                item.add(mapper.map(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return item;
    }
}
