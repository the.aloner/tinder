package ua.danit.dao.jdbc;

import ua.danit.dao.SessionDao;
import ua.danit.mappers.Mapper;
import ua.danit.model.BrowserSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static ua.danit.dao.jdbc.DaoUtils.*;
import static ua.danit.dao.jdbc.UserJdbcDao.USER_TABLE_FIELDS;

public class SessionJdbcDao implements SessionDao {

    private final Mapper<BrowserSession> mapper;
    private final DataSource dataSource;
    private static DaoUtils daoUtils = DAO_UTILS;

    public SessionJdbcDao(Mapper<BrowserSession> mapper, DataSource dataSource) {
        this.mapper = mapper;
        this.dataSource = dataSource;
        DAO_UTILS.setDataSource(dataSource);
    }

    @Override
    public BrowserSession getSessionByToken(String token) {
        String sql = "SELECT sessions.token, " + USER_TABLE_FIELDS +
                " FROM sessions" +
                " JOIN users ON sessions.user_id = users.id" +
                " WHERE token = '" + token + "'";

        return daoUtils.getItemFromQuery(sql, mapper);
    }

    @Override
    public void saveSession(BrowserSession session) {
        String sql = "INSERT INTO sessions (token, user_id)" +
                " VALUES (?, ?);";

        try (Connection con = dataSource.getConnection();
             PreparedStatement statement = con.prepareStatement(sql)) {

            statement.setString(1, session.getToken());
            statement.setLong(2, session.getUser().getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isSessionExist(String token) {
        return nonNull(getSessionByToken(token));
    }

    @Override
    public void remove(String token) {
        if (isNull(token)) {
            throw new IllegalArgumentException();
        }

        String sql = "DELETE FROM sessions" +
                " WHERE token = '" + token + "'";

        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement()) {

            statement.execute(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
