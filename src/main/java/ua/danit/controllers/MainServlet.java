package ua.danit.controllers;

import com.google.common.collect.ImmutableMap;
import ua.danit.model.BrowserSession;
import ua.danit.model.User;
import ua.danit.service.SessionService;
import ua.danit.service.UserService;
import ua.danit.utils.TemplateLoader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.NoSuchElementException;

import static ua.danit.service.SessionService.SESSION_SERVICE;
import static ua.danit.service.UserService.USER_SERVICE;
import static ua.danit.utils.TemplateLoader.TEMPLATE_LOADER;

@WebServlet(name = "mainServlet", urlPatterns = "/*", loadOnStartup = 1)
public class MainServlet extends HttpServlet {

    private final TemplateLoader templateLoader = TEMPLATE_LOADER;
    private final UserService userService = USER_SERVICE;
    private final SessionService sessionService = SESSION_SERVICE;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BrowserSession session = sessionService.getSession(req.getCookies());
        boolean hasUser;

        HashMap<String, Object> templateMap = new HashMap<>();
        templateMap.put("loggedInUser", session.getUser());

        try {
            User user = userService.getRandomUserUnrelatedTo(session.getUser());
            templateMap.put("user", user);
            hasUser = true;
        } catch (NoSuchElementException e) {
            hasUser = false;
        }

        templateMap.put("hasUser", hasUser);

        resp.setContentType("text/html");
        resp.setCharacterEncoding("utf-8");
        PrintWriter out = resp.getWriter();
        templateLoader.write("index.ftl", out, Collections.unmodifiableMap(templateMap));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("/");
    }
}
