package ua.danit.auth;

import com.google.common.collect.ImmutableMap;
import ua.danit.model.BrowserSession;
import ua.danit.model.User;
import ua.danit.service.SessionService;
import ua.danit.service.UserService;
import ua.danit.utils.TemplateLoader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;

import static java.util.Objects.nonNull;
import static ua.danit.service.SessionService.SESSION_SERVICE;
import static ua.danit.service.UserService.USER_SERVICE;
import static ua.danit.utils.TemplateLoader.TEMPLATE_LOADER;

@WebServlet(name = "loginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

    public static final String USER_TOKEN_KEY = "user-token";
    private final TemplateLoader templateLoader = TEMPLATE_LOADER;
    private final UserService userService = USER_SERVICE;
    private final SessionService sessionService = SESSION_SERVICE;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        templateLoader.write("login.ftl", out, ImmutableMap.of(
                "loginFailed", false
        ));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        User user = userService.getUserByEmail(email);

        if (nonNull(user) && user.getPassword().equals(password)) {
            String token = UUID.randomUUID().toString();
            sessionService.saveSession(new BrowserSession(token, user));

            Cookie cookie = new Cookie(USER_TOKEN_KEY, token);
            cookie.setMaxAge(14400);
            resp.addCookie(cookie);
            resp.sendRedirect("/");
        } else {
            resp.setContentType("text/html");
            resp.setCharacterEncoding("utf-8");
            PrintWriter out = resp.getWriter();
            templateLoader.write("login.ftl", out, ImmutableMap.of(
                    "loginFailed", true
            ));
        }
    }
}
