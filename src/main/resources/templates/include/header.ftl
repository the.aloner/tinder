<header class="masthead mb-auto">
    <div class="inner">
        <h3 class="masthead-brand"><a href="/">Danitinder</a></h3>
        <nav class="nav nav-masthead justify-content-center pl-5">
            <a class="nav-link" href="/messages">Messages</a>
            <a class="nav-link" href="/logout">Logout (${loggedInUser.name})</a>
        </nav>
    </div>
</header>