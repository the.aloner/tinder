package ua.danit.dao.jdbc;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import ua.danit.dao.MessageDao;
import ua.danit.mappers.Mapper;
import ua.danit.model.Message;
import ua.danit.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static ua.danit.dao.jdbc.DaoUtils.DAO_UTILS;
import static ua.danit.dao.jdbc.UserJdbcDao.USER_TABLE_FIELDS;

public class MessageJdbcDao implements MessageDao {
    private final Mapper<Message> mapper;
    private final DataSource dataSource;
    private static DaoUtils daoUtils = DAO_UTILS;

    public MessageJdbcDao(Mapper<Message> mapper, DataSource dataSource) {
        this.mapper = mapper;
        this.dataSource = dataSource;
        DAO_UTILS.setDataSource(dataSource);
    }


    @Override
    public List<Message> getMessagesOfTwoUsers(User user1, User user2) {
        String sql = "SELECT messages.id AS message_id, messages.text, " + USER_TABLE_FIELDS +
                " FROM messages" +
                " JOIN users ON users.id = messages.author_id" +
                " WHERE " +
                " relations_actor_id IN (" + user1.getId() + ", " + user2.getId() + ")" +
                " AND " +
                " relations_subject_id IN (" + user1.getId() + ", " + user2.getId() + ")" +
                " ORDER BY messages.id ASC;";
        return daoUtils.getItemListFromQuery(sql, mapper);
    }

    @Override
    public void addMessageFromTo(String message, User author, User recipient) {
        String sql = "INSERT INTO messages" +
                " (author_id, text, relations_actor_id, relations_subject_id)" +
                " VALUES (?, ?, ?, ?);";

        try (Connection con = dataSource.getConnection();
             PreparedStatement statement = con.prepareStatement(sql)) {

            statement.setLong(1, author.getId());
            statement.setString(2, message);
            statement.setLong(3, author.getId());
            statement.setLong(4, recipient.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
