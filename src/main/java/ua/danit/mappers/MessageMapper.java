package ua.danit.mappers;

import ua.danit.model.Message;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MessageMapper implements Mapper<Message> {
    @Override
    public Message map(ResultSet resultSet) throws SQLException {
        return new Message(
                resultSet.getLong("message_id"),
                new UserMapper().map(resultSet),
                resultSet.getString("text")
        );
    }
}
