package ua.danit.controllers;

enum UserActions {
    LIKE ("/like"), SKIP ("/skip");

    private String url;

    UserActions(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
