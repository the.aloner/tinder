package ua.danit.controllers;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import ua.danit.model.SocketMessage;
import ua.danit.model.User;
import ua.danit.service.*;


import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import static ua.danit.service.MessageService.MESSAGE_SERVICE;
import static ua.danit.service.SessionService.SESSION_SERVICE;
import static ua.danit.service.UserService.USER_SERVICE;

@ServerEndpoint(value = "/chatsocket/{user_id}/{token}",
        decoders = SocketMessageDecoder.class,
        encoders = SocketMessageEncoder.class)
public class ChatEndpoint {

    private final UserService userService = USER_SERVICE;
    private final SessionService sessionService = SESSION_SERVICE;
    private final MessageService messageService = MESSAGE_SERVICE;

    private Session session;
    private static Set<ChatEndpoint> chatEndpoints
            = new CopyOnWriteArraySet<>();
    private static HashMap<String, String> users = new HashMap<>();

    @OnOpen
    public void onOpen(Session session, @PathParam("user_id") String userId, @PathParam("token") String token) throws IOException, EncodeException {
        this.session = session;
        chatEndpoints.add(this);
        User sender = sessionService.getUserByToken(token);
        users.put(session.getId(), Long.toString(sender.getId()));

        // debug
        SocketMessage message = new SocketMessage();
        message.setType("message");
        message.setTo(userId);
        message.setContent("Connected!");
        send(message, userId);
        System.out.println("[Chat] New connection " + session.getId() + " userId = " + userId);
    }

    @OnClose
    public void onClose(Session session) throws IOException, EncodeException {
        chatEndpoints.remove(this);
        System.out.println("[Chat] Connection closed " + session.getId());
    }

    @OnMessage
    public void onMessage(Session session, SocketMessage message) throws IOException, EncodeException, DecodeException {

        if ("ping".equals(message.getType())) {
            message.setType("pong");
            send(message, users.get(session.getId()));
            return;
        }

        message.setFrom(users.get(session.getId()));
        send(message, message.getTo());

        long authorId = Long.parseLong(users.get(session.getId()));
        User author = userService.getUserById(authorId);

        long recipientId = Long.parseLong(message.getTo());
        User recipient = userService.getUserById(recipientId);

        messageService.addMessageFromTo(message.getContent(), author, recipient);

        System.out.println("[Chat] Received message from " + session.getId() + ": " + message);
    }

    @OnMessage
    public void onPong(Session session, ByteBuffer message) {
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        System.err.println("[Chat] An error occurred on connection " + session.getId() + ":" + throwable);
        throwable.printStackTrace();
    }

    private static void send(SocketMessage message, String recipient) throws IOException, EncodeException {
        chatEndpoints.forEach(endpoint -> {
            synchronized (endpoint) {
                try {
                    String sessionId = endpoint.session.getId();
                    System.out.println("sessionId " + sessionId);
                    String user = users.get(sessionId);
                    System.out.println("user " + user);
                    if (recipient.equals(user)) {
                        endpoint.session.getBasicRemote().sendObject(message);
                    }
                } catch (IOException | EncodeException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private static void broadcast(SocketMessage message) throws IOException, EncodeException {

        chatEndpoints.forEach(endpoint -> {
            synchronized (endpoint) {
                try {
                    endpoint.session.getBasicRemote().sendObject(message);
                } catch (IOException | EncodeException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}