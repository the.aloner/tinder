package ua.danit.service;

import ua.danit.dao.UserDao;
import ua.danit.dao.jdbc.DataSource;
import ua.danit.dao.jdbc.UserJdbcDao;
import ua.danit.mappers.UserMapper;
import ua.danit.model.User;

import java.util.List;

public class UserService {

    public final static UserService USER_SERVICE = new UserService();
    private static UserDao userDao = new UserJdbcDao(new UserMapper(), new DataSource("amazon-rds"));

    private UserService() {

    }

    public User getRandomUserUnrelatedTo(User user) {
        return userDao.getRandomUserUnrelatedTo(user);
    }

    public User getUserById(long userId) {
        return userDao.getUserById(userId);
    }

    public void likeUser(User actor, User subject) {
        userDao.likeUser(actor, subject);
    }

    public void skipUser(User actor, User subject) {
        userDao.skipUser(actor, subject);
    }

    public boolean haveRelationship(User actor, User subject) {
        return userDao.haveRelationship(actor, subject);
    }

    public List<User> getLikedUsers(User user) {
        return userDao.getLikedUsers(user);
    }

    public List<User> getMutuallyLikedUsers(User user) {
        return userDao.getMutuallyLikedUsers(user);
    }

    public User getUserByEmail(String email) {
        return userDao.getUserByEmail(email);
    }
}
