package ua.danit.dao.jdbc;

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;
import ua.danit.dao.UserDao;
import ua.danit.mappers.UserMapper;
import ua.danit.model.User;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class UserJdbcDaoTest {

    private static UserDao userDao = new UserJdbcDao(new UserMapper(), new DataSource("junit"));

    @Test
    public void getUserByIdShouldReturnNullWithUnknownId() {
        User result = userDao.getUserById(-2L);
        assertNull(result);
    }

    @Test
    public void getUserByIdShouldReturnUser() {
        User expected = new User(2L,
                "Mary",
                "mary@example.com",
                "stronGPass@!",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQPuvDOY8ZlUSsDy0uQQ13mGrTK0VH6EsVyXnUhWy0QphOPF29gBA",
                "F");

        User result = userDao.getUserById(2);

        assertThat(result, is(expected));
    }

    @Test
    public void getUserByEmailShouldReturnNullWithUnknownEmail() {
        User expected = new User(2L,
                "Mary",
                "mary@example.com",
                "stronGPass@!",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQPuvDOY8ZlUSsDy0uQQ13mGrTK0VH6EsVyXnUhWy0QphOPF29gBA",
                "F");

        User result = userDao.getUserByEmail("unknown@example.com");

        assertNull(result);
    }

    @Test
    public void getUserByEmailShouldReturnUser() {
        User expected = new User(2L,
                "Mary",
                "mary@example.com",
                "stronGPass@!",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQPuvDOY8ZlUSsDy0uQQ13mGrTK0VH6EsVyXnUhWy0QphOPF29gBA",
                "F");

        User result = userDao.getUserByEmail("mary@example.com");

        assertThat(result, is(expected));
    }

    @Test
    public void shouldReturnEmptyArrayListIfNoLikedUsers() {
        ArrayList<User> expected = new ArrayList<>();
        User user = new User(5L, "", "", "");
        List<User> result = userDao.getLikedUsers(user);

        assertThat(result, is(expected));
    }

    @Test
    public void shouldReturnEmptyArrayListIfNoUnrelatedUsersLeft() {
        ArrayList<User> expected = new ArrayList<>();
        User user = new User(3L, "", "", "");
        List<User> result = userDao.getUnrelatedUsers(user);

        assertThat(result, is(expected));
    }

    @Test
    public void shouldReturnUnrelatedUsers() {
        long[] expected = {4L, 5L};
        User user = new User(1L, "", "", "");
        List<User> unrelatedUsers = userDao.getUnrelatedUsers(user);
        long[] result = new long[unrelatedUsers.size()];

        for (int i = 0; i < unrelatedUsers.size(); i++) {
            result[i] = unrelatedUsers.get(i).getId();
        }

        assertArrayEquals(result, expected);
    }

    @Test
    public void shouldReturnLikedUsers() {
        long[] expected = {2L, 3L, 6L, 7L};
        User user = new User(1L, "", "", "");
        List<User> likedUsers = userDao.getLikedUsers(user);
        long[] result = new long[likedUsers.size()];

        for (int i = 0; i < likedUsers.size(); i++) {
            result[i] = likedUsers.get(i).getId();
        }

        assertArrayEquals(result, expected);
    }

    @Test
    public void shouldReturnMutuallyLikedUsers() {
        long[] expected = {3L, 6L};
        User user = new User(1L, "", "", "");
        List<User> mutuallyLikedUsers = userDao.getMutuallyLikedUsers(user);
        long[] result = new long[mutuallyLikedUsers.size()];

        for (int i = 0; i < mutuallyLikedUsers.size(); i++) {
            result[i] = mutuallyLikedUsers.get(i).getId();
        }

        assertArrayEquals(result, expected);
    }

    @Test
    public void shouldReturnMutuallyLikedUsers2() {
        long[] expected = {4L};
        User user = new User(2L, "", "", "");
        List<User> mutuallyLikedUsers = userDao.getMutuallyLikedUsers(user);
        long[] result = new long[mutuallyLikedUsers.size()];

        for (int i = 0; i < mutuallyLikedUsers.size(); i++) {
            result[i] = mutuallyLikedUsers.get(i).getId();
        }

        assertArrayEquals(result, expected);
    }

}