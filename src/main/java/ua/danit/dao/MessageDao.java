package ua.danit.dao;

import ua.danit.model.Message;
import ua.danit.model.User;

import java.util.List;

public interface MessageDao {
    List<Message> getMessagesOfTwoUsers(User user1, User user2);

    void addMessageFromTo(String message, User author, User recipient);
}
