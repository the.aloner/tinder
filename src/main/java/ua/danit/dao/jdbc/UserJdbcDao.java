package ua.danit.dao.jdbc;

import ua.danit.dao.UserDao;
import ua.danit.mappers.BooleanMapper;
import ua.danit.mappers.Mapper;
import ua.danit.model.User;

import java.sql.*;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

import static ua.danit.dao.jdbc.DaoUtils.DAO_UTILS;
import static ua.danit.model.User.LIKED;
import static ua.danit.model.User.SKIPPED;

public class UserJdbcDao implements UserDao{

    public final static String USER_TABLE_FIELDS = "users.id AS user_id, users.name AS user_name, users.email AS user_email," +
            " users.password AS user_password, users.image_url AS user_image_url, users.gender AS user_gender";

    private final Mapper<User> mapper;
    private final DataSource dataSource;
    private final DaoUtils daoUtils = DAO_UTILS;

    public UserJdbcDao(Mapper<User> mapper, DataSource dataSource) {
        this.mapper = mapper;
        this.dataSource = dataSource;
        DAO_UTILS.setDataSource(dataSource);
    }

    @Override
    public User getUserById(long userId) {
        String sql = "SELECT " + USER_TABLE_FIELDS + " FROM users" +
                " WHERE users.id = " + userId;

        return daoUtils.getItemFromQuery(sql, mapper);
    }

    @Override
    public User getUserByEmail(String email) {
        String sql = "SELECT " + USER_TABLE_FIELDS + " FROM users" +
                " WHERE users.email = '" + email + "'";

        return daoUtils.getItemFromQuery(sql, mapper);
    }

    @Override
    public User getRandomUserUnrelatedTo(User user) {
        List<User> unrelatedUsers = getUnrelatedUsers(user);

        if (unrelatedUsers.size() > 0) {
            Random random = new Random();
            int randomUser = random.nextInt(unrelatedUsers.size());
            return unrelatedUsers.get(randomUser);
        } else {
            throw new NoSuchElementException("There are no users you can like at this time.");
        }
    }

    public List<User> getUnrelatedUsers(User currentUser) {
        String sql = "SELECT DISTINCT " + USER_TABLE_FIELDS + " FROM users" +
                " LEFT JOIN relations as r ON r.subject_id = users.id" +
                " WHERE" +
                " (r.subject_id IS NULL" +
                " OR" +
                " r.subject_id NOT IN" +
                " (SELECT subject_id FROM relations where actor_id = " + currentUser.getId() + ")" +
                " )" +
                " AND " +
                " users.id != " + currentUser.getId() +
                " ORDER BY users.id ASC";

        return daoUtils.getItemListFromQuery(sql, mapper);
    }

    @Override
    public List<User> getLikedUsers(User currentUser) {
        String sql = "SELECT " + USER_TABLE_FIELDS + " FROM users" +
                " JOIN relations ON relations.subject_id = users.id" +
                " WHERE" +
                " relations.actor_id = " + currentUser.getId() + " AND" +
                " relations.relation = " + LIKED;

        return daoUtils.getItemListFromQuery(sql, mapper);
    }

    @Override
    public List<User> getMutuallyLikedUsers(User user) {
        String sql = "SELECT " + USER_TABLE_FIELDS + " FROM relations AS r1" +
                " LEFT JOIN relations AS r2 ON r1.subject_id = r2.actor_id" +
                " LEFT JOIN users ON r1.subject_id = users.id" +
                " WHERE" +
                " r1.actor_id = r2.subject_id AND" +
                " r1.relation = 1 AND" +
                " r2.relation = 1 AND" +
                " r1.actor_id = " + user.getId();

        return daoUtils.getItemListFromQuery(sql, mapper);
    }

    @Override
    public void likeUser(User actor, User subject) {
        if (haveRelationship(actor, subject)) {
            updateRelation(actor, subject, LIKED);
        } else {
            addRelation(actor, subject, LIKED);
        }
    }

    @Override
    public void skipUser(User actor, User subject) {
        if (haveRelationship(actor, subject)) {
            updateRelation(actor, subject, SKIPPED);
        } else {
            addRelation(actor, subject, SKIPPED);
        }
    }

    private void updateRelation(User actor, User subject, byte relation) {
        try (Connection con = dataSource.getConnection()){
             PreparedStatement statement = con.prepareStatement(
                     "UPDATE relations SET relation = ? " +
                     " WHERE actor_id = ? AND subject_id = ?");
            statement.setByte(1, relation);
            statement.setLong(2, actor.getId());
            statement.setLong(3, subject.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean haveRelationship(User actor, User subject) {
        String sql = "SELECT count(*) as rel FROM relations WHERE" +
                " actor_id = " + actor.getId() +
                " AND subject_id = " + subject.getId();

        return daoUtils.getItemFromQuery(sql, new BooleanMapper());
    }

    private void addRelation(User actor, User subject, byte relation) {
        String sql = "INSERT INTO relations" +
                " VALUES (?, ?, ?);";

        try (Connection con = dataSource.getConnection();
             PreparedStatement statement = con.prepareStatement(sql)) {

            statement.setLong(1, actor.getId());
            statement.setLong(2, subject.getId());
            statement.setByte(3, relation);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
