package ua.danit.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class User {

    public static final byte NO_STATUS = 0;
    public static final byte LIKED = 1;
    public static final byte SKIPPED = 2;

    private long id;
    private String name;
    private String email;
    private String password;
    private String imageUrl;
    private String gender;
    private Map<User, Byte> relations = new HashMap<>();

    public User(long id, String name, String imageUrl, String gender) {
        this.id = id;
        this.name = name;
        this.email = "";
        this.password = "";
        this.imageUrl = imageUrl;
        this.gender = gender;
    }

    public User(long id, String name, String email, String password, String imageUrl, String gender) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.imageUrl = imageUrl;
        this.gender = gender;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getGender() {
        return gender;
    }

    public void likeUser(User subject) {
        if (relations.containsKey(subject)) {
            relations.replace(subject, LIKED);
        } else {
            relations.put(subject, LIKED);
        }
    }

    public void skipUser(User subject) {
        if (relations.containsKey(subject)) {
            relations.replace(subject, SKIPPED);
        } else {
            relations.put(subject, SKIPPED);
        }
    }

    public Map<User, Byte> getRelations() {
        return relations;
    }

    public List<User> getLikedUsers() {
        return this.getRelations().entrySet().stream()
                .filter(e -> e.getValue().equals(LIKED))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public boolean isRelatedTo(User subject) {
        return relations.containsKey(subject);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", password='").append(password).append('\'');
        //sb.append(", imageUrl='").append(imageUrl).append('\'');
        sb.append(", gender='").append(gender).append('\'');
        /*sb.append(", relations=").append(relations);
        sb.append(relations.entrySet().stream()
                .map(e -> e.getKey() + " -> " + e.getValue())
                .collect(Collectors.joining(", \n"))
        );*/
        sb.append("}\n");
        return sb.toString();
    }
}