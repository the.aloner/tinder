package ua.danit.mappers;

import ua.danit.model.BrowserSession;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SessionMapper implements Mapper<BrowserSession>{
    @Override
    public BrowserSession map(ResultSet resultSet) throws SQLException {
        return new BrowserSession(
                resultSet.getString("token"),
                new UserMapper().map(resultSet)
        );
    }
}
