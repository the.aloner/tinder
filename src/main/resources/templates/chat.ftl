<#include "include/layout-chat.ftl" />

<@chatPage title="Chat | Tindr">
<h2>Chat with ${user.name}</h2>
<div class="container" id="chat-scrollable" style="overflow-y: scroll;">
    <div class="container" id="chat-container">
        <#if (messagesNumber > 0) >
        <#list messages as message>
        <div class="float-<#if message.author == user>left<#else>right</#if>">
            ${message.text}
        </div>
        <div class="clearfix"></div>
        </#list>
            <#else>
            <p id="no-messages">There are no messages yet.</p>
        </#if>
    </div>
</div>
<div class="container">
    <div class="mb-3 mt-4">
        <form id="chat-form" action="#" style="display: inherit;">
            <input type="hidden" name="user_id" id="user_id" value="${user.id}">
            <div class="input-group">
                <input type="text" name="content" id="content" class="form-control" placeholder="Type your message">
                <div class="input-group-append">
                    <button type="submit" class="btn btn-primary">Send</button>
                </div>
            </div>
        </form>
    </div>
</div>
</@chatPage>