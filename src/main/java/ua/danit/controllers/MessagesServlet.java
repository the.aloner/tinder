package ua.danit.controllers;

import com.google.common.collect.ImmutableMap;
import ua.danit.model.BrowserSession;
import ua.danit.model.User;
import ua.danit.service.SessionService;
import ua.danit.service.UserService;
import ua.danit.utils.TemplateLoader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static ua.danit.service.SessionService.SESSION_SERVICE;
import static ua.danit.service.UserService.*;
import static ua.danit.utils.TemplateLoader.*;

@WebServlet (urlPatterns = "/messages", name = "messagesServlet")
public class MessagesServlet extends HttpServlet {

    private static UserService userService = USER_SERVICE;
    private static SessionService sessionService = SESSION_SERVICE;
    private static TemplateLoader templateLoader = TEMPLATE_LOADER;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BrowserSession session = sessionService.getSession(req.getCookies());
        List<User> mutuallyLikedUsers = userService.getMutuallyLikedUsers(session.getUser());

        resp.setContentType("text/html");
        resp.setCharacterEncoding("utf-8");
        PrintWriter out = resp.getWriter();
        templateLoader.write("messages.ftl", out, ImmutableMap.of(
                "users", mutuallyLikedUsers,
                "usersNumber", mutuallyLikedUsers.size(),
                "loggedInUser", session.getUser()
        ));
    }
}
