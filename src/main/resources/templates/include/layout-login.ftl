<#macro loginPage title=""><#include "head.ftl">
<body class="text-center">
<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
        <#nested/>
    <footer class="mastfoot mt-auto">
        <div class="inner"></div>
    </footer>
</div>
    <#include "footer.ftl">
</body>
</html>
</#macro>