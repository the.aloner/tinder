package ua.danit.dao.jdbc;

import org.junit.Test;
import ua.danit.dao.MessageDao;
import ua.danit.mappers.MessageMapper;
import ua.danit.model.Message;
import ua.danit.model.User;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class MessageJdbcDaoTest {

    private static MessageDao messageDao = new MessageJdbcDao(new MessageMapper(), new DataSource("junit"));

    @Test
    public void getMessagesOfTwoUsers() {
        List<Message> expected = new ArrayList<>();
        User user1 = new User(1L,
                "Edward",
                "edward@example.com",
                "123456",
                "https://cdn.iconscout.com/public/images/icon/free/png-128/avatar-cry-face-laugh-lol-smile-smiley-emoji-3bd33f5f427fc3d6-128x128.png",
                "M");
        User user2 = new User(3L,
                "Johanna",
                "a@example.com",
                "rtj84prjrth",
                "https://www.ihdimages.com/wp-content/uploadsktz/2017/09/cute-girl-1.jpg",
                "F");
        expected.add(new Message(1L, user2, "Hi"));
        expected.add(new Message(2L, user1, "Hello there"));
        expected.add(new Message(3L, user2, "How are you?"));
        expected.add(new Message(4L, user1, "I'm good, thanks"));

        List<Message> result = messageDao.getMessagesOfTwoUsers(user1, user2);

        assertThat(result, is(expected));
    }

    @Test
    public void numberOfMessagesBetweenTwoUsersShouldBeIncrementedWhenNewMessageAdded() {
        User author = new User(1L,
                "Edward",
                "edward@example.com",
                "123456",
                "https://cdn.iconscout.com/public/images/icon/free/png-128/avatar-cry-face-laugh-lol-smile-smiley-emoji-3bd33f5f427fc3d6-128x128.png",
                "M");
        User recipient = new User(3L,
                "Johanna",
                "a@example.com",
                "rtj84prjrth",
                "https://www.ihdimages.com/wp-content/uploadsktz/2017/09/cute-girl-1.jpg",
                "F");

        List<Message> before = messageDao.getMessagesOfTwoUsers(author, recipient);
        messageDao.addMessageFromTo("a message", author, recipient);
        List<Message> after = messageDao.getMessagesOfTwoUsers(author, recipient);

        assertThat(after.size(), is(before.size() + 1));
    }
}