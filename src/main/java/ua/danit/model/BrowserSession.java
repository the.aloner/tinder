package ua.danit.model;


public class BrowserSession {
    private final String token;
    private final User user;

    public BrowserSession(String token, User user) {
        this.token = token;
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public User getUser() {
        return user;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("BrowserSession{");
        sb.append(", token='").append(token).append('\'');
        sb.append(", user=").append(user);
        sb.append('}');
        return sb.toString();
    }
}
