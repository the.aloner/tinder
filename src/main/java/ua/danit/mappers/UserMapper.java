package ua.danit.mappers;

import ua.danit.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements Mapper<User> {
    @Override
    public User map(ResultSet resultSet) throws SQLException {
        return new User(
                resultSet.getLong("user_id"),
                resultSet.getString("user_name"),
                resultSet.getString("user_email"),
                resultSet.getString("user_password"),
                resultSet.getString("user_image_url"),
                resultSet.getString("user_gender")
        );
    }
}
