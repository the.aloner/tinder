package ua.danit.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BooleanMapper implements Mapper<Boolean> {
    @Override
    public Boolean map(ResultSet resultSet) throws SQLException {
        int rel = resultSet.getInt("rel");
        return rel > 0;
    }
}
