package ua.danit.auth;

import ua.danit.service.SessionService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.util.Objects.nonNull;
import static ua.danit.service.SessionService.SESSION_SERVICE;

@WebServlet(name = "logoutServlet", urlPatterns = "/logout")
public class LogoutServlet extends HttpServlet{

    private final SessionService sessionService = SESSION_SERVICE;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logout(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logout(req, resp);
    }

    private void logout(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Cookie[] cookies = req.getCookies();
        Cookie loggedInUserCookie = sessionService.getLoggedInUserCookie(cookies);

        if (nonNull(loggedInUserCookie)) {
            sessionService.remove(loggedInUserCookie.getValue());
            loggedInUserCookie.setMaxAge(0);
            resp.addCookie(loggedInUserCookie);
        }

        resp.sendRedirect("/");
    }
}
