package ua.danit.dao;

import ua.danit.model.User;

import java.util.List;

public interface UserDao {

    User getUserById(long userId);
    User getUserByEmail(String email);
    User getRandomUserUnrelatedTo(User user);
    void likeUser(User actor, User subject);
    void skipUser(User actor, User subject);
    boolean haveRelationship(User actor, User subject);
    List<User> getLikedUsers(User currentUser);
    List<User> getMutuallyLikedUsers(User user);
    List<User> getUnrelatedUsers(User currentUser);
}
