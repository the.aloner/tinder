<#include "include/layout-login.ftl" />

<@loginPage title="Messages | Tindr">
    <header class="masthead mb-auto">
        <div class="inner">
            <h3 class="masthead-brand" style="float: inherit;">Danitinder</h3>
        </div>
    </header>

    <main role="main" class="inner cover">
        <form class="form-signin" action="/login" method="post">
            <p class="h3 mb-3 font-weight-normal">Please sign in</p>
    <#if loginFailed>
        <div class="alert alert-warning" role="alert">
            Incorrect email or password
        </div>
    </#if>
            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="email" id="email" name="email" class="form-control" placeholder="Email address" required
                   autofocus>
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2018</p>
        </form>
    </main>


</@loginPage>