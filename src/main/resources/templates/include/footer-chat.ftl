<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script>
    $(function() {
        connect();
        updateScroll();
    });

    var ws;
    var missed_heartbeats;

    function connect() {
        var user_id = $("#user_id").val();
        var host = "${host}";
        ws = new WebSocket("ws://" + host + "/chatsocket/" + user_id + "/" + $.cookie("user-token"));
        var ping_msg = '{"type":"ping"}', pong_msg = '{"type":"ping"}', heartbeat_interval = null;
        missed_heartbeats = 0;

        ws.onopen = function(event) {
            if (heartbeat_interval === null) {
                heartbeat_interval = setInterval(function() {
                    try {
                        missed_heartbeats++;
                        if (missed_heartbeats >= 3)
                            throw new Error("Too many missed heartbeats.");
                        ws.send(ping_msg);
                    } catch(e) {
                        clearInterval(heartbeat_interval);
                        heartbeat_interval = null;
                        console.warn("Closing connection. Reason: " + e.message);
                        ws.close();
                    }
                }, 20000);
            }
        };

        ws.onmessage = function(event) {
            console.log(event.data);
            var message = JSON.parse(event.data);

            if (message.type == "pong") {
                missed_heartbeats = 0;
                return;
            }

            $("#no-messages").empty();

            if (message.from == user_id) {
                $("#chat-container").append("" +
                        "<div class=\"float-left\">\n" +
                        message.content +
                        "</div>\n" +
                        "<div class=\"clearfix\"></div>");
                updateScroll();
            }
        };

    }

    $("#chat-form").submit(function (e) {
        send();
        addMessageToChat();
        clearChatForm();
        return false;
    });

    function ping() {
        ws.send('{"content":"~~~*ping*~~~"}');
    }

    function send() {
        var content = $("#content").val();
        var to_id = $("#user_id").val();

        var json = JSON.stringify({
            "type":"message",
            "content":content,
            "to":to_id
        });
        console.log(json);

        ws.send(json);
    }

    function addMessageToChat() {
        var message = $("#content").val();
        $("#chat-container").append("" +
                "<div class=\"float-right\">\n" +
                message +
                "</div>\n" +
                "<div class=\"clearfix\"></div>");
        updateScroll();
    }

    function clearChatForm() {
        $("#content").val("");
    }

    function updateScroll() {
        $("#chat-scrollable").scrollTop($("#chat-scrollable")[0].scrollHeight);
    }
</script>