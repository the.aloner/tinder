package ua.danit.service;

import ua.danit.dao.MessageDao;
import ua.danit.dao.jdbc.DataSource;
import ua.danit.dao.jdbc.MessageJdbcDao;
import ua.danit.mappers.MessageMapper;
import ua.danit.model.Message;
import ua.danit.model.User;

import java.util.List;

public class MessageService {

    public final static MessageService MESSAGE_SERVICE = new MessageService();
    private static MessageDao messageDao = new MessageJdbcDao(new MessageMapper(), new DataSource("amazon-rds"));

    private MessageService() {
    }

    public List<Message> getMessagesOfTwoUsers(User user1, User user2) {
        return messageDao.getMessagesOfTwoUsers(user1, user2);
    }

    public void addMessageFromTo(String message, User author, User recepient) {
        messageDao.addMessageFromTo(message, author, recepient);
    }
}
