package ua.danit.dao;

import ua.danit.model.BrowserSession;

public interface SessionDao {

    void saveSession(BrowserSession session);
    BrowserSession getSessionByToken(String token);
    boolean isSessionExist(String token);
    void remove(String token);
}
