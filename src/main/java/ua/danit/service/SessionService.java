package ua.danit.service;

import ua.danit.dao.SessionDao;
import ua.danit.dao.jdbc.DataSource;
import ua.danit.dao.jdbc.SessionJdbcDao;
import ua.danit.mappers.SessionMapper;
import ua.danit.model.BrowserSession;
import ua.danit.model.User;

import javax.servlet.http.Cookie;

import static java.util.Objects.nonNull;
import static ua.danit.auth.LoginServlet.USER_TOKEN_KEY;

public class SessionService {

    public final static SessionService SESSION_SERVICE = new SessionService();
    private static SessionDao sessionDao = new SessionJdbcDao(new SessionMapper(), new DataSource("amazon-rds"));

    private SessionService() {
    }


    public void saveSession(BrowserSession session) {
        sessionDao.saveSession(session);
    }

    public BrowserSession getSession(Cookie[] cookies) {
        Cookie cookie = getLoggedInUserCookie(cookies);
        return getSessionByToken(cookie.getValue());
    }

    public BrowserSession getSessionByToken(String token) {
        return sessionDao.getSessionByToken(token);
    }

    public boolean isSessionExist(String token) {
        return sessionDao.isSessionExist(token);
    }

    public void remove(String token) {
        sessionDao.remove(token);
    }

    public boolean isLoggedIn(Cookie[] cookies) {
        boolean isLoggedIn = false;

        if (nonNull(cookies)) {
            for (Cookie cookie : cookies) {
                if (USER_TOKEN_KEY.equals(cookie.getName())&& isSessionExist(cookie.getValue())) {
                    isLoggedIn = true;
                    break;
                }
            }
        }

        return isLoggedIn;
    }

    public Cookie getLoggedInUserCookie(Cookie[] cookies) {
        Cookie userCookie = null;

        if (nonNull(cookies)) {
            for (Cookie cookie : cookies) {
                if (USER_TOKEN_KEY.equals(cookie.getName()) && isSessionExist(cookie.getValue())) {
                    userCookie = cookie;
                    break;
                }
            }
        }

        return userCookie;
    }

    public User getUserByToken(String token) {
        BrowserSession session = getSessionByToken(token);
        return session.getUser();
    }
}
