package ua.danit.dao.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import static ua.danit.utils.PropertiesReader.PROPERTIES_READER;

public class DataSource {

    private final String dbUrl;
    private final String dbUser;
    private final String dbPassword;

    public DataSource(String environment) {
        Properties properties = PROPERTIES_READER.getPropertiesForEnvironment(environment);
        dbUrl = properties.getProperty("dbUrl");
        dbUser = properties.getProperty("dbUser");
        dbPassword = properties.getProperty("dbPassword");
    }

    public Connection getConnection() throws SQLException {
        Connection connection;

        if (System.getProperty("RDS_HOSTNAME") != null) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                throw new RuntimeException("Cannot find the driver in the classpath!", e);
            }

            connection = getRDSConnection();
        } else {
            connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
        }

        return connection;
    }

    private static Connection getRDSConnection() throws SQLException {
        String dbName = System.getProperty("RDS_DB_NAME");
        String userName = System.getProperty("RDS_USERNAME");
        String password = System.getProperty("RDS_PASSWORD");
        String hostname = System.getProperty("RDS_HOSTNAME");
        String port = System.getProperty("RDS_PORT");
        String jdbcUrl = "jdbc:mysql://" + hostname + ":" + port + "/" + dbName + "?user=" + userName + "&password=" + password + "&useUnicode=true&characterEncoding=utf8";
        Connection con = DriverManager.getConnection(jdbcUrl);
        return con;
    }
}
