package ua.danit.model;

import java.util.Objects;

public class Message {
    private final long id;
    private final User author;
    private final String text;

    public Message(long id, User author, String text) {
        this.id = id;
        this.author = author;
        this.text = text;
    }

    public long getId() {
        return id;
    }

    public User getAuthor() {
        return author;
    }

    public String getText() {
        return text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return id == message.id &&
                Objects.equals(author, message.author) &&
                Objects.equals(text, message.text);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, author, text);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Message{");
        sb.append("id=").append(id);
        sb.append(", author=").append(author.toString());
        sb.append(", text='").append(text).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
