package ua.danit.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertiesReader {

    public static final PropertiesReader PROPERTIES_READER = new PropertiesReader();
    private static final Map<String, String> ENV_TO_PROPERTIES_FILE;

    static {
        ENV_TO_PROPERTIES_FILE = new HashMap<>();
        ENV_TO_PROPERTIES_FILE.put("application", "application.properties");
        ENV_TO_PROPERTIES_FILE.put("junit", "junit.properties");
        ENV_TO_PROPERTIES_FILE.put("amazon-rds", "amazon-rds.properties");
    }

    private PropertiesReader() {}

    public Properties getPropertiesForEnvironment(String env) {
        Properties properties = new Properties();
        String propertyFile = ENV_TO_PROPERTIES_FILE.get(env);

        try (InputStream input = getClass().getClassLoader().getResourceAsStream(propertyFile)) {
            properties.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return properties;
    }
}
