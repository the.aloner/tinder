package ua.danit.controllers;

import ua.danit.model.BrowserSession;
import ua.danit.model.User;
import ua.danit.service.SessionService;
import ua.danit.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static ua.danit.controllers.UserActions.*;
import static ua.danit.controllers.UserActions.SKIP;
import static ua.danit.service.SessionService.SESSION_SERVICE;
import static ua.danit.service.UserService.USER_SERVICE;

@WebServlet(name = "userServlet", urlPatterns = "/user/*")
public class UserActionServlet extends HttpServlet {

    private final UserService userService = USER_SERVICE;
    private static SessionService sessionService = SESSION_SERVICE;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BrowserSession session = sessionService.getSession(req.getCookies());
        User loggedInUser = session.getUser();

        long subjectId = Long.parseLong(req.getParameter("user_id"));
        User subject = userService.getUserById(subjectId);

        String pathInfo = req.getPathInfo();

        if (LIKE.getUrl().equals(pathInfo)) {
            userService.likeUser(loggedInUser, subject);
        } else if (SKIP.getUrl().equals(pathInfo)) {
            userService.skipUser(loggedInUser, subject);
        }

        resp.sendRedirect("/");
    }


}