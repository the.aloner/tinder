<#include "include/layout-default.ftl" />

<@standardPage title="Home | Tindr">
    <main role="main" class="inner cover">
        <#if hasUser>
            <#if user??>
            <h1 class="cover-heading">${user.name}</h1>
            <img alt="Photo" style="height: 500px;" class="rounded mx-auto d-block" src="${user.imageUrl}">

            <div class="mt-3">
                <div class="float-left">
                    <form action="/user/like" method="post">
                        <input type="hidden" name="user_id" value="${user.id}">
                        <button type="submit" class="btn btn-success">Like</button>
                    </form>
                </div>
                <div class="float-right">
                    <form action="/user/skip" method="post">
                        <input type="hidden" name="user_id" value="${user.id}">
                        <button type="submit" class="btn btn-danger">Skip</button>
                    </form>
                </div>
            </div>
            </#if>
        <#else>
            <h2>There are no users you can like at this time</h2>
            <h4>Try again later</h4>
        </#if>
    </main>
</@standardPage>