package ua.danit.auth;

import ua.danit.service.SessionService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static ua.danit.service.SessionService.*;

@WebFilter (urlPatterns = "/*")
public class LoginFilter implements Filter {

    private final SessionService sessionService = SESSION_SERVICE;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        Cookie[] cookies = req.getCookies();
        boolean isLoginPage = req.getRequestURI().equals("/login");
        boolean isLoggedIn = sessionService.isLoggedIn(cookies);
        boolean isAsset = req.getRequestURI().startsWith("/assets");

        if (isLoggedIn || isLoginPage || isAsset) {
            filterChain.doFilter(req, resp);
        } else {
            resp.sendRedirect("/login");
        }
    }

    @Override
    public void destroy() {

    }
}
