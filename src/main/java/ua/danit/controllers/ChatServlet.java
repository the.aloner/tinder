package ua.danit.controllers;

import com.google.common.collect.ImmutableMap;
import ua.danit.model.Message;
import ua.danit.model.BrowserSession;
import ua.danit.model.User;
import ua.danit.service.MessageService;
import ua.danit.service.SessionService;
import ua.danit.service.UserService;
import ua.danit.utils.TemplateLoader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.InvalidParameterException;
import java.util.List;

import static ua.danit.service.MessageService.MESSAGE_SERVICE;
import static ua.danit.service.SessionService.SESSION_SERVICE;
import static ua.danit.service.UserService.USER_SERVICE;
import static ua.danit.utils.TemplateLoader.*;

@WebServlet (urlPatterns = "/messages/*")
public class ChatServlet extends HttpServlet {

    private static TemplateLoader templateLoader = TEMPLATE_LOADER;
    private static UserService userService = USER_SERVICE;
    private static MessageService messageService = MESSAGE_SERVICE;
    private static SessionService sessionService = SESSION_SERVICE;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BrowserSession session = sessionService.getSession(req.getCookies());

        String pathInfo = req.getPathInfo().substring(1);
        long userId = parseUserIdFrom(pathInfo);

        User talkPartner = userService.getUserById(userId);
        User loggedInUser = session.getUser();
        List<Message> messages = messageService.getMessagesOfTwoUsers(loggedInUser, talkPartner);

        String host;

        if (System.getProperty("AWS_HOSTNAME") != null) {
            host = System.getProperty("AWS_HOSTNAME");
        } else {
            host = "localhost:8080";
        }

        resp.setContentType("text/html");
        resp.setCharacterEncoding("utf-8");
        PrintWriter out = resp.getWriter();
        templateLoader.write("chat.ftl", out, ImmutableMap.of(
                "user", talkPartner,
                "loggedInUser", loggedInUser,
                "messages", messages,
                "messagesNumber", messages.size(),
                "host", host
        ));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String message = req.getParameter("message");

        String userIdString = req.getParameter("user_id");
        long userId = parseUserIdFrom(userIdString);
        User recipient = userService.getUserById(userId);

        BrowserSession session = sessionService.getSession(req.getCookies());
        User author = session.getUser();

        if (userService.haveRelationship(author, recipient)) {
            messageService.addMessageFromTo(message, author, recipient);
        } else {
            System.out.println("Users are not related");
        }

        resp.sendRedirect("/messages/" + userId);
    }

    private long parseUserIdFrom(String userIdString) {
        long userId;

        try {
            userId = Long.parseLong(userIdString);
        } catch (Exception e) {
            throw new InvalidParameterException("Invalid user ID");
        }

        return userId;
    }
}
