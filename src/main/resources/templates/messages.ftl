<#include "include/layout-default.ftl" />

<@standardPage title="Messages | Tindr">
    <#if (usersNumber > 0) >
        <h1>Chat with</h1>
        <ul>
        <#list users as user>
            <div class="card" style="width: 18rem; background-color: inherit; border: none; padding-bottom: 10px;">
                <a href="/messages/${user.id}">
                    <img class="card-img-top" style="width: 100px;" src="${user.imageUrl}" alt="Card image cap">
                    <div class="card-body">
                        ${user.name}
                    </div>
                </a>
            </div>
        </#list>
        </ul>
    <#else>
    <h2>You have no mutual relationships yet</h2>
    <h4>Try liking more people</h4>
    </#if>
</@standardPage>