package ua.danit.service;

import com.google.gson.Gson;
import ua.danit.model.SocketMessage;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

public class SocketMessageDecoder implements Decoder.Text<SocketMessage> {

    private static Gson gson = new Gson();

    @Override
    public SocketMessage decode(String s) throws DecodeException {
        return gson.fromJson(s, SocketMessage.class);
    }

    @Override
    public boolean willDecode(String s) {
        return (s != null);
    }

    @Override
    public void init(EndpointConfig endpointConfig) {
        // Custom initialization logic
    }

    @Override
    public void destroy() {
        // Close resources
    }
}
